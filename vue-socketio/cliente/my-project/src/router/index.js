import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Counter from '@/components/Counter'
import Agentes from '@/components/Agentes'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/counter',
      name: 'counter',
      component: Counter
    },
    {
      path: '/Agentes',
      name: 'Agentes',
      component: Agentes
    }
  ]
})
