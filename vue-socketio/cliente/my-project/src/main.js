// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuex from 'vuex'
import VueSocketio from 'vue-socket.io'
import agentelist from './modules/Agentelist'
import counterModule from './modules/counter'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    io: {}
  },
  mutations: {
    setSocket: (state, socket) => {
      state.io = socket
      console.log('socket conectado')
    }
  },
  modules: {
    counterModule,
    agentelist
  }
})

Vue.use(VueSocketio, 'http://localhost:5000', store)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  beforeCreate () {
    store.commit('setSocket', this.$socket)
    store.state.agentelist.list.push({
      name: 'agente123',
      active: false
    })
  },
  router,
  components: { App },
  template: '<App/>'
})
