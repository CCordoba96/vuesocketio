const Agent = {
  data: {
    agents: ['ag1', 'ag2']
  },
  methods: {
    addAgent (name) {
      Agent.data.agents.push(name)
    }
  }
}
export default Agent
