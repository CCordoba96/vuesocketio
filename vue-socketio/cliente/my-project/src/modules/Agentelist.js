/* eslint-disable no-undef */
const agentelist = {
  state: {
    list: []
  },
  mutations: {
    SOCKET_AGENT_ADD (state, names) {
      state.list.push({
        name: names[0],
        active: false
      })
    }

  },
  actions: {
    socket_activate: ({state, rootState}, indexe) => {
      rootState.io.emit('activate', state.list[indexe].name)
      state.list[indexe].active = true
    },
    socket_desactivate: ({state, rootState}, indexe) => {
      rootState.io.emit('desactivate', state.list[indexe].name)
      state.list[indexe].active = false
    }
  },
  getters: {
    list (state) {
      return state.list
    }
  }
}
export default agentelist
