from aiohttp import web
import socketio
from flask import Flask, render_template, request
sio = socketio.AsyncServer()
app = web.Application()
app = Flask(__name__)
sio.attach(app)
import ibmiotf.application
import uuid
import json
from uuid import getnode as get_mac
#####################################
#FILL IN THESE DETAILS
#####################################
organization = "cac5px"
deviceType = "broker"
deviceId = "broker12345"
appId = str(uuid.uuid4())
authMethod = "token"
authToken = "b0Lbw*1s1z5R(friL@"

##API TOKEN AND KEY
authkey = "a-cac5px-uwi800evqs"
authtoken = "4V7MfFUjxC_AXDvlUv"
# Initialize the application client.
appOptions = {"org": organization, "id": appId,"auth-method": "apikey", "auth-key" : authkey, "auth-token":authtoken }
appCli = ibmiotf.application.Client(appOptions)

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode='gevent')

def myAppEventCallback(event):
	print("Received live data from %s (%s) sent at %s: %s" % (event.deviceId, event.deviceType, event.timestamp.strftime("%H:%M:%S"), json.dumps(event.data)))
	socketio.emit('SOCKET_AGENT_ADD',"agente",room="hola")
	print("enviado")

@app.route('/')
def index():
    return render_template('index.html')



@sio.on('activate')
def print_messasge( message):
    print("activate")
	
    print("Socket ID: " , message)

@sio.on('desactivate')
def print_messasge( message):
    print("desactivate")
    join_room("hola")
    print("Socket ID: " , message)

@sio.on('connect')
def print_message(*sid):
    if(sid != None):
        print (sid)
    print(request.namespace )
    join_room("hola")

@sio.on('increment')
def increment(state):
    print ("increment")
    join_room("hola")
    emit('COUNTER_INCREMENT',int(state)+1,room="hola")

    emit('AGENT_ADD',"agente1",room="hola")

@sio.on('decrement')
async def decrement(state):
    print ("decrement")
    print(state)
    await emit('COUNTER_DECREMENT',int(state)-1,room="hola")

    await sio.emit('COUNTER_DECREMENT',int(state)-1)
if __name__ == '__main__':
    appCli.connect()
    appCli.deviceEventCallback = myAppEventCallback
    appCli.subscribeToDeviceEvents(event="1-register")
    web.run_app(app,port=5000)
