from flask import Flask, render_template, request, session
from flask_socketio import SocketIO, emit,join_room
from iopa import ACLMessage
import pymongo
from pymongo import MongoClient
from iopa import AID
import ibmiotf.application
import uuid
import json
from uuid import getnode as get_mac
import time
from threading import Thread
import logging
"""
Datos necesarios para conectarnos al broker mqtt
"""
organization = "cac5px"
deviceType = "broker"
deviceId = "broker12345"
appId = str(uuid.uuid4())
authMethod = "token"
authToken = "b0Lbw*1s1z5R(friL@"
authkey = "a-cac5px-uwi800evqs"
authtoken = "4V7MfFUjxC_AXDvlUv"
# Initialize the application client.
appOptions = {"org": organization, "id": appId,"auth-method": "apikey", "auth-key" : authkey, "auth-token":authtoken }
appCli = ibmiotf.application.Client(appOptions,logHandlers=[logging.NullHandler()])

"""
CONEXION A MONGODB
"""
uri="mongodb://admin:IBKWQNCGJXRUTALX@sl-eu-fra-2-portal.4.dblayer.com:15869,sl-eu-fra-2-portal.5.dblayer.com:15869/compose?authSource=admin"
client = MongoClient(uri)
db=client['Platform-1']
collection=db.Agents
"""
Conectar flask y socketIO
"""
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

def insert(obj):

    collection.insert_one(obj)

def AMS(ACLMsg):
    newaclm=ACLMessage.ACLMessage(None,ACLMsg)
    a=newaclm.getContent()

    if a['action']=="register":
        sender=newaclm.getSender()
        print(sender.getName())
        agente={"_id":sender.getName(),"address":sender.getAddress(),"state":"registered"}
        try:
            insert(agente)
        except pymongo.errors.DuplicateKeyError:
            print("error, agente con la misma id")

        #Aquí añadir error si el id no es único
        socketio.emit('AGENT_ADD',sender.getName(),room="hola")
    else:
        print("Otras acciones, deregister...")


def myAppEventCallback(event):
    print("Received live data from %s (%s) sent at %s: %s" % (event.deviceId,event.deviceType,event.timestamp.strftime("%H:%M:%S"),json.dumps(event.data)))
    print(event.data)
    if event.event == "a-AMS":
        AMS(event.data)


    """

    """

@app.route('/')
def index():
    return render_template('index.html')

@socketio.on('activate')
def print_messasge( message):
    print("activate")
	
    print("Activar agente: " , message)

@socketio.on('desactivate')
def print_messasge( message):
    print("desactivate")
    join_room("hola")
    print("Socket ID: " , message)

@socketio.on('connect')
def print_message(*sid):
    if(sid != None):
        print (sid)
    print(request.sid)
    print(session)
    join_room("hola")

@socketio.on('increment')
def increment(state):
    print ("increment")

    emit('COUNTER_INCREMENT',int(state)+1)

    emit('AGENT_ADD',"agente1")

@socketio.on('decrement')
def decrement(state):
    print ("decrement")
    print(state)
    emit('COUNTER_DECREMENT',int(state)-1)



if __name__ == '__main__':
    appCli.connect()
    appCli.deviceEventCallback = myAppEventCallback
    appCli.subscribeToDeviceEvents(event="a-AMS")
    socketio.run(app,debug=True)
