
class Behaviour():
    def __init__(self):
        self._running=False

    def setAgent(self,agent):
        self.myAgent=agent

    def onStart(self):
        pass

    def onEnd(self):
        pass

    def kill(self):
        """
        self.forcekill=threading.Event()
        self.forcekill.set()
        stops the behaviour
        :return:
        """
    def _process(self):
        """
        Main loop of the behaviour
        must be overriden
        :return:
        """
        raise NotImplementedError
    def run(self):
        if not self._running:
            self._running=True
        else:
            #it is just running
            return
        self.onStart()

        #try:
        while(not self.done()):
            self._process()

        #except Exception as e:
           # print ("Exception in behaviour "+ str(self)+ ": "+str(e))
        self.onEnd()

    def done(self):
        """
        return if the behaviour has finished
        :return:
        """
        return False
