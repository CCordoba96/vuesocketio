import json
class AID():
    def __init__(self,name=None,addres=None):
        if name is not None:
            self.__name=name
        else:
            self.__name=""

        if addres is not None:
            self.address=addres
        else:
            self.address=""
    def getName(self):
        return self.__name

    def setName(self,name):
        self.__name=name
    def getAddress(self):
        return self.address
    def setAddress(self,address):
        self.address=address
    def match(self,other):
        """
        return True if two AIDs are similar
        else returns False
        :param other:
        :return:
        """
        if other is None:
            return True
        elif other.getName == self.__name & other.getAddress == self.getAddress():
            return True
        else:
            return False
    def __eq__(self,other):
        """
        return True if two AIDs are similar
        else returns False
        :param other:
        :return:
        """
        if other is None:
            return True
        elif other.getName == self.__name:
            return True
        else:
            return False
    def asJSON(self):
        msg = {"AID": self.__name}
        msg["Address"]=self.address
        return json.dumps(msg)
    def loadJSON(self,jsonstring):
        jsonstring=json.loads(json.dumps(jsonstring))
        self.setName(jsonstring["AID"])
        self.setAddress(jsonstring["Address"])