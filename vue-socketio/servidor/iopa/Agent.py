import os
import ibmiotf.application
import uuid
import queue
import logging
from iopa import ACLMessage
from iopa import AID
from iopa import Behaviour
import time
import json
class Agent():
    def __init__(self,apikey,name):
        self._name=name
        self._apikey=apikey
        self.connect()
        self.finish=False
        self.activebehaviours=[]
        self.messageQueue=queue.Queue()
        self.aid=AID.AID(self._name,self._apikey+"-"+self._name)
        self.register()

    def register(self):

        aclm=ACLMessage.ACLMessage('inform')
        aclm.setSender(self.aid)
        aid=AID.AID("AMS","AMS")
        aclm.addReceiver(aid)
        msg={"action":"register"}
        aclm.setContent(msg)
        print(aclm.asJSON())
        self.send(aclm)
    def myAppEventCallback(self):
        """
        El problema es que llega al agente, como pasarle cada vez que llega un mensaje a los behaviours activos

        :return:
        """
        pass

    def _setup(self):
        raise NotImplementedError

    def send(self,aclmessage):
        for aid in aclmessage.getReceivers():
            endpoint=aid.getAddress()
            newaclm=ACLMessage.ACLMessage(aclmessage.getPerformative())
            newaclm.setSender(self.aid)
            newaclm.addReceiver(aid)
            newaclm.setContent(aclmessage.getContent())
            diccio=(newaclm.asJSON())
            print(endpoint)
            for x in range(0, 10):
                data = {'hello': 'world', 'x': x}
                diccio["data"]=x
                def myOnPublishCallback():
                    print("Confirmed event %s received by IoTF\n" % x)
                self.appCli.publishEvent(self.deviceType,self.deviceId,endpoint,"json",diccio,qos=2,on_publish=myOnPublishCallback())
                time.sleep(1)

    def run(self):
        self._setup()
        """
        Aqui deberíamos meterle algo de los mensajes que nos lleguen
        """
        while self.finish != True:
            x=1+1

    def doDelete(self):
        """
        Deberíamos Modificar takedown para que termine los comportamientos que tenemos
        :return:
        """
        print("Finishing agent")
        self.finish=True
    def getName(self):
        return self._name
    def connect(self):
        self.organization = "cac5px"
        self.deviceType = "broker"
        self.deviceId = "broker12345"
        self.appId = str(uuid.uuid4())
        self.authMethod = "token"
        self.authToken = "b0Lbw*1s1z5R(friL@"

        ##API TOKEN AND KEY
        self. authkey = "a-cac5px-uwi800evqs"
        self.authtoken = "4V7MfFUjxC_AXDvlUv"
        self.appOptions = {"org": self.organization, "id": self.appId, "auth-method": "apikey", "auth-key": self.authkey,
                      "auth-token": self.authtoken}
        self.appCli = ibmiotf.application.Client(self.appOptions,logHandlers=[logging.NullHandler()])
        self.appCli.connect()
        self.appCli.deviceEventCallback = self.myAppEventCallback
        self.appCli.subscribeToDeviceEvents(event=self._apikey+"-"+self._name)
        """
            Pasarlo por linea de comandos o por archivo de configuracion o ambos
                :return: 
                """


    def addBehaviour(self,behaviour):
        behaviour.setAgent(self)
        behaviour.run()
        self.activebehaviours.append(behaviour)



