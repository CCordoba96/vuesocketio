from iopa import AID
import json
class ACLMessage():
    def __init__(self,performative=None,jsonstring=None):
        self._attrs={}
        #possible FIPA communicative acts
        self.commacts = ['accept-proposal', 'agree', 'cancel',
                         'cfp', 'call-for-proposal', 'confirm', 'disconfirm',
                         'failure', 'inform', 'not-understood',
                         'propose', 'query-if', 'query-ref',
                         'refuse', 'reject-proposal', 'request',
                         'request-when', 'request-whenever', 'subscribe',
                         'inform-if', 'proxy', 'propagate']
        if performative is not None:
            self._attrs["performative"]=performative.lower()

        self.sender=None
        self.receivers=[]
        self.content=None
        if jsonstring:
            self.loadJSON(jsonstring)

    def setSender(self,sender):
        self.sender=sender
    def getSender(self):
        return self.sender

    def addReceiver(self,recv):
        """
        set the sender (AID)
        :param recv:
        :return:
        """
        self.receivers.append(recv)

    def removeReceiver(self,recv):
        """
        removes a receiver from the list (AID)
        :param recv:
        :return:
        """
        if recv in self.receivers:
            self.receivers.remove(recv)

    def getReceivers(self):
        return self.receivers

    def setPerformative(self, p):
        """
        sets the message performative (string)
        must be in ACLMessage.commacts
        """
        # we do not check if is a fipa performative
        # any string is valid...
        #if p and (p.lower() in self.commacts):
        self._attrs["performative"] = p.lower()

    def getPerformative(self):
        """
        returns the message performative (string)
        """

        return (self._attrs['performative'])

    def setContent(self, c):
        """
        sets the message content (string, bytestream, ...)
        """
        self.content = c
    def getContent(self):
        return self.content


    def asJSON(self):
        """
               returns a JSON version of the message
               """
        p={}
        p["performative"]=str(self.getPerformative())
        if self.sender:
            p["sender"]=self.sender.asJSON()
        if self.receivers:
            v=[]
            for i in self.receivers:
                v.append(i.asJSON())
            p["receivers"]=v
        if self.content:
            print(type(self.content))
            if type(self.content) is dict:
                p["content"] = json.dumps(self.content)
            else:
                p["content"] = self.content

        return p

    def createReply(self):
        m=ACLMessage()
        m.setPerformative(self.getPerformative())
        m.addReceiver(self.getSender())
        return m
    def loadJSON(self, jsonstring):
        """
        Lee un archivo Json que recibimos
        :param jsonstring:
        :return:
        """
        p = json.loads(json.dumps(jsonstring))

        if "performative" in p:
            self.setPerformative(p['performative'])
        if "sender" in p:
            aid=AID.AID()
            a=(p["sender"])
            pa=json.loads(a)
            aid.loadJSON(pa)
            self.setSender(aid)
        if "receivers" in p:
            for i in p["receivers"]:
                s = AID.AID()
                pa=json.loads(i)
                s.loadJSON(pa)
                self.addReceiver(s)
        if "content" in p:

            self.setContent(json.loads(p["content"]))


